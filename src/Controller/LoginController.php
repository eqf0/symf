<?php
namespace App\Controller;

use App\Entity\User;
use App\Form\Type\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends AbstractController {
  private UserRepository $repo;

  public function __construct(UserRepository $repo)
  {
    $this -> repo = $repo;
  }

  /**
   * @Route("/login")
   */
  public function login(Request $request, SessionInterface $sess) {
    $user = new User();
    $userForm = $this -> createForm(UserType::class, $user);
    $userForm -> handleRequest($request);

    if ($userForm -> isSubmitted() && $userForm -> isValid()) {
      if ($this -> repo -> validate($user -> getUsername(), $user -> getPassword())) {
        $sess -> set('status', true);
        $sess -> set('username', $user -> getUsername());
        return $this -> redirect('/');
      }
      return $this -> redirect('/login');
    }
    return $this -> render('login.html.twig', ['form' => $userForm -> createView()]);
  }
}
?>

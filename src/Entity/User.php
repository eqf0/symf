<?php
namespace App\Entity;

class User 
{
  protected $username;
  protected $password;

  public function getUsername(): string {
   return $this -> username;
  }

  public function setUsername($n_username) {
   $this -> username = $n_username;
  }

  public function getPassword(): string {
   return $this -> password;
  }

  public function setPassword($n_password) {
   $this -> password = $n_password;
  }

}
?>
